/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula1508;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class FXML_TabelaController implements Initializable {

    @FXML
    private TableView<Pessoa> tabela;
    @FXML
    private TableColumn<Pessoa, String> nome;
    @FXML
    private TableColumn<Pessoa, Integer> idade;
    @FXML
    private TableColumn<Pessoa, String> endereco;
    @FXML
    private TableColumn<Pessoa, String> funcao;
    @FXML
    private RadioButton funcionario;
    @FXML
    private RadioButton professor;
    @FXML
    private RadioButton aluno;
    @FXML
    private TextField textFuncao;
    @FXML
    private TextField textEndereco;
    @FXML
    private TextField textSemestre;
    @FXML
    private TextField textIdade;
    @FXML
    private TextField textNome;
    @FXML
    private TextField textCurso;
    @FXML
    private TextField textSalario;
    @FXML
    private TextField textDisciplina;
    @FXML
    private TextField textSetor;
    @FXML
    private Button cadastrar;
    @FXML
    private Text labelNome;
    @FXML
    private Text labelIdade;
    @FXML
    private Text labelFuncao;
    @FXML
    private Text labelEndereco;
    @FXML
    private Text labelSemestre;
    @FXML
    private Text labelCurso;
    @FXML
    private Text labelSalario;
    @FXML
    private Text labelDisciplina;
    @FXML
    private Text labelSetor;
   
    ArrayList <Pessoa> info = new ArrayList<>();
    @FXML
    private TableColumn<Pessoa, String> semestre;
    @FXML
    private TableColumn<Pessoa, Integer> salario;
    @FXML
    private TableColumn<Pessoa, String> setor;
    @FXML
    private TableColumn<Pessoa, String> disciplina;
    @FXML
    private TableColumn<Pessoa, String> curso;
    @FXML
    private ToggleGroup rbs;
    @FXML
    private Button delete;
    private ObservableList <Pessoa> infos(){
        
        Aluno al = new Aluno();
        al.setNome("Arthur");
        al.setIdade(18);
        al.setCurso("DS");
        al.setEndereco("Petunias");
        al.setSemestre("Sexto");
        
        info.add(al);
        
        Professor prof = new Professor();
        prof.setNome("Joice");
        prof.setIdade(35);
        prof.setDisciplina("Portugues");
        prof.setSalario(5000);
        prof.setEndereco("Açucena");
        info.add(prof);
        
        Funcionario func = new Funcionario();
        func.setNome("Cláudio");
        func.setIdade(57);
        func.setSetor("TI");
        func.setEndereco("Caramuru");
        
        info.add(func);
        
        tabela.setItems(FXCollections.observableList(info));
       return FXCollections.observableList(info);
       
    }
    private int func;
  @FXML
  private void aluno (ActionEvent ev){
        labelNome.setVisible(true);
        labelIdade.setVisible(true);
        labelFuncao.setVisible(false);
        labelEndereco.setVisible(true);
        labelSemestre.setVisible(true);
        labelCurso.setVisible(true);
        labelSalario.setVisible(false);
        labelDisciplina.setVisible(false);
        labelSetor.setVisible(false);
        
        textFuncao.setVisible(false);
        textEndereco.setVisible(true);
        textSemestre.setVisible(true);
        textIdade.setVisible(true);
        textNome.setVisible(true);
        textCurso.setVisible(true);
        textSalario.setVisible(false); 
        textDisciplina.setVisible(false);
        textSetor.setVisible(false);
        
        func = 1;
  }
    @FXML
  private void professor (ActionEvent ev){
        labelNome.setVisible(true);
        labelIdade.setVisible(true);
        labelFuncao.setVisible(false);
        labelEndereco.setVisible(true);
        labelSemestre.setVisible(false);
        labelCurso.setVisible(false);
        labelSalario.setVisible(true);
        labelDisciplina.setVisible(true);
        labelSetor.setVisible(false);
        
        textFuncao.setVisible(false);
        textEndereco.setVisible(true);
        textSemestre.setVisible(true);
        textIdade.setVisible(true);
        textNome.setVisible(true);
        textCurso.setVisible(false);
        textSalario.setVisible(true); 
        textDisciplina.setVisible(true);
        textSetor.setVisible(false);
        
        func=2;
  }
        @FXML
    private void funcionario (ActionEvent ev){
        labelNome.setVisible(true);
        labelIdade.setVisible(true);
        labelFuncao.setVisible(true);
        labelEndereco.setVisible(true);
        labelSemestre.setVisible(false);
        labelCurso.setVisible(false);
        labelSalario.setVisible(true);
        labelDisciplina.setVisible(false);
        labelSetor.setVisible(true);
        
        textFuncao.setVisible(true);
        textEndereco.setVisible(true);
        textSemestre.setVisible(false);
        textIdade.setVisible(true);
        textNome.setVisible(true);
        textCurso.setVisible(false);
        textSalario.setVisible(true); 
        textDisciplina.setVisible(false);
        textSetor.setVisible(true);
        func=3;
  }
    public void cadastra(){
       if(func==1){
       Aluno al = new Aluno();
       
       al.setSemestre(textSemestre.getText());
       al.setCurso(textCurso.getText());
       al.setEndereco(textEndereco.getText());
       al.setNome(textNome.getText());
       al.setIdade(Integer.parseInt(textIdade.getText()));
       //al.inserir();
       }
       
       if(func==2){
       Professor prof = new Professor();
       
       prof.setSalario(Integer.parseInt(textSalario.getText()));
       prof.setDisciplina(textDisciplina.getText());
       prof.setEndereco(textEndereco.getText());
       prof.setNome(textNome.getText());
       prof.setIdade(Integer.parseInt(textIdade.getText()));
       //prof.inserir();
       }
       
        if(func==3){
       Funcionario funci = new Funcionario();
       
       funci.setSalario(Integer.parseInt(textSemestre.getText()));
       funci.setSetor(textSetor.getText());
       funci.setEndereco(textEndereco.getText());
       funci.setNome(textNome.getText());
       funci.setIdade(Integer.parseInt(textIdade.getText()));
       //funci.inserir();
       }
        tabela.refresh();
        //hkj
    }
    @FXML
    public void delete(){
        info.remove(tabela.getSelectionModel().getSelectedItem());
        tabela.refresh();
    }
    @FXML
    public void alterar(){
        
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       // infos = tabela.getItems();
       tabela.setItems(infos());
        
       nome.setCellValueFactory(new PropertyValueFactory <>("Nome"));
       idade.setCellValueFactory(new PropertyValueFactory <>("Idade"));
       endereco.setCellValueFactory(new PropertyValueFactory <>("Endereco"));
       funcao.setCellValueFactory(new PropertyValueFactory <>("Funcao"));
       curso.setCellValueFactory(new PropertyValueFactory<>("Curso"));
       semestre.setCellValueFactory(new PropertyValueFactory<>("Semestre"));
       setor.setCellValueFactory(new PropertyValueFactory<>("Setor"));
       salario.setCellValueFactory (new PropertyValueFactory<>("Salario"));
       disciplina.setCellValueFactory (new PropertyValueFactory<>("disciplina"));
    }    
  
}
